<?php

namespace App\Http\Middleware;

use Closure;

class Localization
{
    public function handle($request, Closure $next)
    {
        $headers = $request->headers->all();

        $languages = collect($headers["accept-language"] ?? ['en'])->first();
        $return = explode(',', explode(';', $languages)[0])[0];

        app()->setLocale(strtolower($return));

        return $next($request);
    }
}
